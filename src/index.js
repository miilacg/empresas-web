import React from 'react';
import ReactDOM from 'react-dom';
/*Faz o caminho entre as páginas*/
import { BrowserRouter, Switch, Route } from "react-router-dom"; 
import Home from './pages/Home/index';
import Login from './pages/Login/index';
import PaginaErro from './pages/Erro';
import './index.css';


ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/" component={ Login } exact/> 
      <Route path="/home" component={ Home }/>
      <Route component={ PaginaErro }/>
    </Switch>       
  </BrowserRouter>,
  document.getElementById('root') /*renderiza o react no root do index.html*/
);