const URL = 'http://empresas.ioasys.com.br/api/v1/users/auth/sign_in';

function valida(objetoDoUsuario){
    return fetch(`${URL}`, {
        method: 'POST', 
        headers:{
            'Content-type': 'application/json',
        },
        body: JSON.stringify(objetoDoUsuario), //converte para texto para rede conseguir entender
    }) //busca os dados
        .then(async(respostaServidor) =>  { 
            //verificar se o servidor está funcionado
            if (respostaServidor.ok) { 
                const resposta = await respostaServidor.json();
                return resposta;
            }    
            throw new Error('Não foi possível pegar os dados :(');
        });
}

export default {
    valida,
};