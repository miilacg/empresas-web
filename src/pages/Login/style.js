import styled from 'styled-components';

export const Login = styled.div`
    width: auto;
    height: auto;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    padding: 1vw 0vw;
`;

export const BemVindo = styled.h1`
    width: 20vw;
    font-size: 2vw;
    font-weight: bold;
    text-transform: uppercase;
    text-align: center;
    color: #383743;

    display: flex;
    align-self: center;
    margin: 4vw auto 1.8vw auto;
`;

export const Texto = styled.h2`
    width: 25vw;
    font-size: 1.3vw;
    font-weight: normal;
    line-height: 1.44;
    text-align: center;
    color: #383743;
    margin: 0vw auto 3vw auto;
`;

export const Button = styled.button` 
    width: 23vw;
    height: 4vw;
    border-radius: 3.6px;
    background-color: var(--greeny-blue);
    border: none;

    font-size: 1.4vw;
    font-weight: bold;
    font-stretch: normal;
    text-align: center;
    color: white;

    margin: 3.5vw 1.35vw 0vw 1.35vw; 
    align-items: center;
    justify-content: center;
    display: flex;
    
    &:hover,
    &:focus {
        opacity: .5;
`;