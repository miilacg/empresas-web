import React, { Component, useEffect, useState } from 'react';
import FormField from '../../componentes/FormField';
import Logo from '../../componentes/Icones/logo';
import useForm from '../../hooks/useForm';
import usuariosRepositorio from '../../repositorio/usuarios';
import email from '../../imagens/ic-email.svg';
import senha from '../../imagens/ic-cadeado.svg';
import olho from '../../imagens/olho.svg';
import '../../../src/index.css';
import { Login, BemVindo, Texto, Button } from './style';

function validaUsuario(infosDoEvento){
    infosDoEvento.preventDefault(); 

    const valoresIniciais = {
        login: '',
        senha: '',
    }

    const { validaUsuario, valores } = useForm({ valoresIniciais });
    const [usuarios, setUsuarios] = useState([]); 

    //chama quando quer que algum efeito colateral aconteça
    useEffect(() => {
        const URL = 'http://empresas.ioasys.com.br/api/v1/users/auth/sign_in';
        fetch(URL) //busca os dados
            .then(async(respostaServidor) =>  { 
                const resposta = await respostaServidor.json();
                setUsuarios([
                    ...resposta,
                ]);
            });
    },[]);   

    usuariosRepositorio.valida({
        login: valores.login,
        senha: valores.password,
    })
}

class PageLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSenhaActive: false,
        };
    }    
    
    alteraIsSenha = () => { /*altera o valor de isSenha*/
        this.setState({
            isSenha: true            
        }) 
    }

    alteraIsSenhaActive = () => { /*altera o valor de isSenhaActive*/
        this.setState({
            isSenhaActive: !this.state.isSenhaActive            
        }) 
    }
  
    renderVisualizarSenha = () => {        
        if (this.state.isSenha) { 
            if (this.state.isSenhaActive) { 
                return (
                    <FormField 
                        iconEsquerda={ senha }
                        placeholder="Senha"
                        type="text"
                        name="senha"
                        iconButton={ olho }
                        acao={ this.alteraIsSenhaActive }
                        alt="ver senha"
                        classNameButton="olho"
                        value={ this.valores.senha }
                        onChange={ this.validaUsuario }
                    />
                )
            }

            return (
                <FormField 
                    iconEsquerda={ senha }
                    placeholder="Senha"
                    type="password"
                    name="senha"
                    iconButton={ olho }
                    acao={ this.alteraIsSenhaActive }
                    alt="ocultar senha"
                    classNameButton="olho"
                    value={ this.valores.senha }
                    onChange={ this.validaUsuario }
                />
            )
        }        

        return (
            <FormField 
                iconEsquerda={ senha }
                placeholder="Senha"
                type="password"
                name="senha"
                onkeyup={ this.alteraIsSenha }
                value={ this.valores.senha }
                onChange={ this.validaUsuario }           
            /> 
        )
    }
    
    render() {       
        return (
            <Login>
                <Logo className="Logo" srcset="../../imagens/logo-home@2x.png 2x, ../../imagens/logo-home@3x.png 3x"></Logo>
    
                <BemVindo>Bem-vindo ao empresas</BemVindo>
    
                <Texto>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</Texto>
    
                <form onSubmit = { validaUsuario() }>

                    <FormField 
                        iconEsquerda={ email }
                        placeholder="E-mail"
                        type="email"
                        name="emailLogin"
                        value={ valores.login }
                        onChange={ this.validaUsuario }
                    />  

                    { this.renderVisualizarSenha() }
    
                    <Button type="submit" className="botao">Entrar</Button>                
                </form>    
            </Login>        
        );
    }
}

export default PageLogin;