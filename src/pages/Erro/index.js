import React from 'react';
import styled from 'styled-components';
import logo from '../../imagens/logo-home.png';
import '../../../src/index.css';

const Container = styled.div`
    width: auto;
    height: auto;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    padding: 0vw;
`;

const Logo = styled.img`
    width: 22vw;
    height: auto;
    display: flex;
    align-self: center;
    margin: auto;

    @media (max-width: 985px) {
        width: 26vw;
    }

    @media (max-width: 700px) {
        width: 30vw;
    }

    @media (max-width: 450px) {
        width: 35vw;
    }
`;

const Erro = styled.h1`
    font-size: 2.8vw;
    font-weight: bold;
    text-align: center;
    color: #383743;
    margin-left: 1vw;

    @media (max-width: 985px) {
        font-size: 3.5vw;
        margin-left: 1.5vw;
    }

    @media (max-width: 700px) {
        font-size: 4vw;
        margin-left: 2.35vw;
    }
`;


function PageLogin(){
    return (
        <Container>
            <Logo className="Logo" src={logo} alt="logo ioasys"></Logo>

            <Erro>Essa página não existe.</Erro> 
        </Container>        
    )
}

export default PageLogin;