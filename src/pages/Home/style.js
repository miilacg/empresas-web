import styled from 'styled-components';

export const Texto = styled.p`
  font-size: 2.5vw;
  line-height: 1.22;
  letter-spacing: -0.45px;
  color: #383743;

  width: auto;
  height: auto;
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;

export const Logo = styled.img`
  width: 18vw;

  display: flex;
  align-item: center;
  margin: auto;
`;

export const Button = styled.button`
    background: transparent;
    border: none;
    width: auto;
    height: auto;
    padding: 0;

    &:focus{
        outline: none;
    }
`;