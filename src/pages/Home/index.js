import React, { Component } from 'react';
import styled from 'styled-components';
import FormField from '../../componentes/FormField';
import Menu from '../../componentes/Menu';
import Lupa from '../../componentes/Icones/lupa.js';
import logo from '../../imagens/logo-nav.png';
import lupa from '../../imagens/ic-search-copy.svg';
import fechar from '../../imagens/ic-close.svg';
import { Texto, Logo, Button } from './style';


class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPesquisaActive: false
    }
  }


  alteraPesquisa = () => { /*altera o valor de isPesquisaActive*/
    this.setState({
      isPesquisaActive: !this.state.isPesquisaActive
    }) 
    console.log(this.state.isSenhaActive);
  }

  renderAcaoLupa = () => {
    if (this.state.isPesquisaActive) {
      const Menu = styled.nav`
        width: 100%;
        height: 10vw;
        z-index: 100;

        display: flex;
        align-items: flex-end;

        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        padding: 1.5% 2.5%;        
      `;      

      return (
        <Menu>          
          <FormField 
            iconEsquerda={ lupa }
            placeholder="Pesquisar"
            type="text"
            name="textoPesquisa"
            iconButton={ fechar }
            acao={ this.alteraPesquisa }
            alt="fechar pesquisa"
            classNameButton="fechar"
          />
        </Menu>
      )
    }
    return (
      <Button onClick={ this.alteraPesquisa }>
        <Lupa/>
      </Button>
    )
  }


  render() {
    return (
      <>
        <Menu >
          <Logo style={{ display: this.state.isPesquisaActive ? 'none' : '' }} src={logo} alt="logo ioasys"></Logo> 
          { this.renderAcaoLupa() }                  
        </Menu>

        <main>
          <Texto style={{ display: this.state.isPesquisaActive ? 'none' : '' }} >Clique na busca para iniciar.</Texto>
        </main>
      </>
    );
  }
}

export default Home;