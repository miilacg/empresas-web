import React from 'react';
import { FormFieldWrapper, Input, Button, Imagem } from './style';
import './style.css';

function FormField({ iconEsquerda, placeholder, type, name, onkeyup, onchange, value, iconDireita, alt, acao, iconButton, classNameButton }) {
  const fildId = `id_${name}`;
  
  return (
    <FormFieldWrapper style={{ margin: type !== 'text' ? '0 0 35px 0' : '' }}>
      <img src={ iconEsquerda }/>      
      <Input        
        placeholder={ placeholder }
        id={ fildId }
        type={ type }
        name={ name }
        onKeyUp={ onkeyup }
        onChange={ onchange }
        value={ value }
        required        
      />
      <img src={ iconDireita } />

      <Button onClick={ acao }>
        <Imagem className={ classNameButton } src={ iconButton } alt={ alt }></Imagem> 
      </Button> 
    </FormFieldWrapper>
  );
}

export default FormField;