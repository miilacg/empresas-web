import styled from 'styled-components';


export const FormFieldWrapper = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
  
    input[name='textoPesquisa']{
        width: 100%;
        background: transparent;
        padding: 0 .6vw;
        margin: 0;

        font-size: 2.35vw;
        opacity: 1;
        text-align: left;
        color: #991237;
    }

    &:not([type='text']){
        border-bottom: solid 0.7px #383743;
    }
`;

export const Input = styled.input`
  background: var(--beige);
  color: #383743;
  display: block;
  width: 100%;
  outline: 0;
  border: 0;
  border: transparent; 

  padding: 0vw .5vw;  

  resize: none;
  transition: border-color .3s;  

  opacity: 0.5;
  font-size: 1.3vw;
  letter-spacing: -0.25px;
  text-align: left;  
`;

export const Button = styled.button`
    background: transparent;
    border: none;
    width: auto;
    height: auto;
    padding: 0;

    &:focus{
        outline: none;
    }
`;

export const Imagem = styled.img`
    &:focus{
        outline: none;
    }
`;