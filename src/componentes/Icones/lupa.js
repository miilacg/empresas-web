import React from 'react';
import styled from 'styled-components';
import lupa from '../../imagens/ic-search-copy.svg';

const ImagemLupa = styled.img`
    width: 3.5vw;

    &:focus{
        outline: none;
    }
`;

function Lupa(){
    return(
        <ImagemLupa src={lupa} alt="pesquisar"></ImagemLupa>
    );
}

export default Lupa;