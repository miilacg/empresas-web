import React from 'react';
import styled from 'styled-components';
import logo from '../../imagens/logo-home.png';

const ImagemLogo = styled.img`
    width: 23vw;
    height: auto;
    object-fit: contain;

    display: flex;
    align-self: center;
    margin: 0vw auto 4vw auto;
`;

function Logo(){
    return(
        <ImagemLogo src={logo} alt="logo ioasys"></ImagemLogo>
    );
}

export default Logo;