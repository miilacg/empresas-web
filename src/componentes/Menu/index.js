import React from 'react';
import styled from 'styled-components';

export const Nav = styled.nav`
  width: 100%;
  height: 10vw;
  z-index: 100;

  display: flex;
  align-items: center;

  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  padding-left: 5%;
  padding-right: 5%;

  background-color: var(--medium-pink);
  background-image: linear-gradient(to bottom, transparent, rgba(13, 4, 48, .2));
`;

function Menu({ children }){
    return (
        <Nav>
            { children } 
        </Nav>
    );
}


export default Menu;