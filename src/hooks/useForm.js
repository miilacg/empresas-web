import { useState } from 'react';

function useForm (valoresIniciais){
    const [valores, setValores] = useState(valoresIniciais); 

    function setValor(chave, valor){
        //chave é um valor variavel
        setValores ({
            ...valores,
            [chave]: valor, 
        })
    }

    function trocaDadoUsuario(infosDoEvento) {
        setValor(
            infosDoEvento.target.getAttribute('name'),
            infosDoEvento.target.value
        );
    }

    function clearForm(){
        setValores(valoresIniciais);
    }

    return {
        valores,
        trocaDadoUsuario,
        clearForm,
    };
}

export default useForm;